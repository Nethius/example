## 4.0.0.7 (2021-09-21)

No changes.

## 1.0.3 (2021-08-31)

### deprecated (2 changes)

- [deprecated(#13446): Прекращена поддержка android с версией ниже 6.0](Nethius/example@7efb95802f240e83c4fb293e50aa36f032c5d234) ([merge request](Nethius/example!4))
- [deprecated(#13444): Прекращена поддержка версий ниже 1.0.0](Nethius/example@fa3fcfb43143dbac3b45d2061dec2a39afeed67a) ([merge request](Nethius/example!4))

### bug (6 changes)

- [bug(#12445): Исправлено зависание приложения, при нажатии кнопки "Выход"](Nethius/example@e7289be2484ab6464b30c349fa6af9f4c8b2379c) ([merge request](Nethius/example!4))
- [bug(#12347): Исправлено отображение кнопок "Добавить", "Удалить"](Nethius/example@3f731b9e9788d372a78c8f2d0a92813745601ada) ([merge request](Nethius/example!4))
- [bug(#12445): Исправлено зависание приложения, при нажатии кнопки "Выход"](Nethius/example@7706104787d6bd0b13f7efba8be5093b7bdc24cd)
- [bug(#12347): Исправлено отображение кнопок "Добавить", "Удалить"](Nethius/example@d703b23a518d3f8f681579f41a67be0711d28be6)
- [bug(#12445): Исправлено зависание приложения, при нажатии кнопки "Выход"](Nethius/example@d8c351f1cf34cb89b72f86d49873d6753eff2bd9)
- [bug(#12347): Исправлено отображение кнопок "Добавить", "Удалить"](Nethius/example@a456f8244358018a231567a3bfc09901b8714639)

### performance (6 changes)

- [refactoring(#12349): Повышена скорость сборки приложения](Nethius/example@c97360e87d291851e46f6a42f89d967e151c08a7) ([merge request](Nethius/example!4))
- [refactoring(#12348): Повышена скорость загрузки приложения](Nethius/example@9c3810017a2b4c72e025daf369ddc91303b839f2) ([merge request](Nethius/example!4))
- [refactoring(#12349): Повышена скорость сборки приложения](Nethius/example@cfd1fec08009e85bcf38159919a2ef166dd604f5)
- [refactoring(#12348): Повышена скорость загрузки приложения](Nethius/example@0bb2c4927188af3918a3a5cbebbd9df10b2e7902)
- [refactoring(#12349): Повышена скорость сборки приложения](Nethius/example@92e8059aed38ff8a548c83c473986d4d2a67d10c) ([merge request](Nethius/example!2))
- [refactoring(#12348): Повышена скорость загрузки приложения](Nethius/example@6825581bfef8400f6f0edf4a682e3ab1a3e68364)

### feature (6 changes)

- [feature(#12346): Добавлена поддержка файлов формата .md .yml](Nethius/example@435d9477afd8f06bc35f7f6e0cf85f4bde14f806) ([merge request](Nethius/example!4))
- [feature(#12345): Реализована форма авторизации](Nethius/example@c2f7941467ff69580114a74c199929b215a273ec) ([merge request](Nethius/example!4))
- [feature(#12346): Добавлена поддержка файлов формата .md .yml](Nethius/example@3b7fc3b25b52461fa76e2741bda521f7bff1d149)
- [feature(#12345): Реализована форма авторизации](Nethius/example@fffef210d76100fc1ceaae4c30d327f51b8899c9)
- [feature(#12346): Добавлена поддержка файлов формата .md .yml](Nethius/example@1482513b832c2a331f612cb52499af0ba5907b7f)
- [feature(#12345): Реализована форма авторизации](Nethius/example@b6bca4a30acd33706a393c93ab021844acea5776)

## 1.0.2 (2021-08-31)

### Features (6 changes)

- [feature(#12346): Добавлена поддержка файлов формата .md .yml](Nethius/example@435d9477afd8f06bc35f7f6e0cf85f4bde14f806) by @Nethius

- [feature(#12345): Реализована форма авторизации](Nethius/example@c2f7941467ff69580114a74c199929b215a273ec) by @Nethius

- [feature(#12346): Добавлена поддержка файлов формата .md .yml](Nethius/example@3b7fc3b25b52461fa76e2741bda521f7bff1d149) by @Nethius

- [feature(#12345): Реализована форма авторизации](Nethius/example@fffef210d76100fc1ceaae4c30d327f51b8899c9) by @Nethius

- [feature(#12346): Добавлена поддержка файлов формата .md .yml](Nethius/example@1482513b832c2a331f612cb52499af0ba5907b7f) by 

- [feature(#12345): Реализована форма авторизации](Nethius/example@b6bca4a30acd33706a393c93ab021844acea5776) by 


### Bug fixes (6 changes)

- [bug(#12445): Исправлено зависание приложения, при нажатии кнопки "Выход"](Nethius/example@e7289be2484ab6464b30c349fa6af9f4c8b2379c) by @Nethius

- [bug(#12347): Исправлено отображение кнопок "Добавить", "Удалить"](Nethius/example@3f731b9e9788d372a78c8f2d0a92813745601ada) by @Nethius

- [bug(#12445): Исправлено зависание приложения, при нажатии кнопки "Выход"](Nethius/example@7706104787d6bd0b13f7efba8be5093b7bdc24cd) by @Nethius

- [bug(#12347): Исправлено отображение кнопок "Добавить", "Удалить"](Nethius/example@d703b23a518d3f8f681579f41a67be0711d28be6) by @Nethius

- [bug(#12445): Исправлено зависание приложения, при нажатии кнопки "Выход"](Nethius/example@d8c351f1cf34cb89b72f86d49873d6753eff2bd9) by @Nethius

- [bug(#12347): Исправлено отображение кнопок "Добавить", "Удалить"](Nethius/example@a456f8244358018a231567a3bfc09901b8714639) by 


### Performance improvements (6 changes)

- [refactoring(#12349): Повышена скорость сборки приложения](Nethius/example@c97360e87d291851e46f6a42f89d967e151c08a7) by @Nethius

- [refactoring(#12348): Повышена скорость загрузки приложения](Nethius/example@9c3810017a2b4c72e025daf369ddc91303b839f2) by @Nethius

- [refactoring(#12349): Повышена скорость сборки приложения](Nethius/example@cfd1fec08009e85bcf38159919a2ef166dd604f5) by @Nethius

- [refactoring(#12348): Повышена скорость загрузки приложения](Nethius/example@0bb2c4927188af3918a3a5cbebbd9df10b2e7902) by @Nethius

- [refactoring(#12349): Повышена скорость сборки приложения](Nethius/example@92e8059aed38ff8a548c83c473986d4d2a67d10c) by 

- [refactoring(#12348): Повышена скорость загрузки приложения](Nethius/example@6825581bfef8400f6f0edf4a682e3ab1a3e68364) by 


### Deprecated features (2 changes)

- [deprecated(#13446): Прекращена поддержка android с версией ниже 6.0](Nethius/example@7efb95802f240e83c4fb293e50aa36f032c5d234) by 

- [deprecated(#13444): Прекращена поддержка версий ниже 1.0.0](Nethius/example@fa3fcfb43143dbac3b45d2061dec2a39afeed67a) by

## 1.0.1 (2021-08-31)

### Features (2 changes)

- [feature(#12346): Добавлена поддержка файлов формата .md .yml](Nethius/example@435d9477afd8f06bc35f7f6e0cf85f4bde14f806) by @Nethius

- [feature(#12345): Реализована форма авторизации](Nethius/example@c2f7941467ff69580114a74c199929b215a273ec) by @Nethius


### Bug fixes (2 changes)

- [bug(#12445): Исправлено зависание приложения, при нажатии кнопки "Выход"](Nethius/example@e7289be2484ab6464b30c349fa6af9f4c8b2379c) by @Nethius

- [bug(#12347): Исправлено отображение кнопок "Добавить", "Удалить"](Nethius/example@3f731b9e9788d372a78c8f2d0a92813745601ada) by @Nethius


### Performance improvements (2 changes)

- [refactoring(#12349): Повышена скорость сборки приложения](Nethius/example@c97360e87d291851e46f6a42f89d967e151c08a7) by @Nethius

- [refactoring(#12348): Повышена скорость загрузки приложения](Nethius/example@9c3810017a2b4c72e025daf369ddc91303b839f2) by @Nethius


### deprecated (2 changes)

- [deprecated(#13446): Прекращена поддержка android с версией ниже 6.0](Nethius/example@7efb95802f240e83c4fb293e50aa36f032c5d234) by 

- [deprecated(#13444): Прекращена поддержка версий ниже 1.0.0](Nethius/example@fa3fcfb43143dbac3b45d2061dec2a39afeed67a) by

## 1.0.0 (2021-08-31)

### bug (2 changes)

- [bug(#12445): Исправлено зависание приложения, при нажатии кнопки "Выход"](Nethius/example@7706104787d6bd0b13f7efba8be5093b7bdc24cd)
- [bug(#12347): Исправлено отображение кнопок "Добавить", "Удалить"](Nethius/example@d703b23a518d3f8f681579f41a67be0711d28be6)

### performance (2 changes)

- [refactoring(#12349): Повышена скорость сборки приложения](Nethius/example@cfd1fec08009e85bcf38159919a2ef166dd604f5)
- [refactoring(#12348): Повышена скорость загрузки приложения](Nethius/example@0bb2c4927188af3918a3a5cbebbd9df10b2e7902)

### feature (2 changes)

- [feature(#12346): Добавлена поддержка файлов формата .md .yml](Nethius/example@3b7fc3b25b52461fa76e2741bda521f7bff1d149)
- [feature(#12345): Реализована форма авторизации](Nethius/example@fffef210d76100fc1ceaae4c30d327f51b8899c9)
